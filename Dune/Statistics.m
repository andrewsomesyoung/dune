//
//  Statistics.m
//  Dune
//
//  Created by Andrew Young on 7/15/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import "Statistics.h"

@interface Statistics()
{
    CFAbsoluteTime _startTime;
    NSUInteger _frameCount;
    NSUInteger _bitCount;
    CGSize _lastResolution;
}
@end

@implementation Statistics

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _startTime = -1;
        _frameCount = 0;
    }
    return self;
}

- (CGSize) resolution
{
    return _lastResolution;
}

- (float) fps
{
    CFAbsoluteTime t = CFAbsoluteTimeGetCurrent();
    
    return _frameCount / (t - _startTime);
}

- (NSUInteger) bitrate
{
    CFAbsoluteTime t = CFAbsoluteTimeGetCurrent();
    
    return _bitCount / (t - _startTime);
}

- (void) capturedFrameWithResolution:(CGSize)resolution
{
    // Start timing on first frame
    if (_startTime == -1)
    {
        _startTime = CFAbsoluteTimeGetCurrent();
    }
    
    _frameCount++;
    _lastResolution = resolution;
}

- (void) encodedSlicesWithSize:(NSUInteger)size
{
    // Start timing on first slice
    if (_startTime == -1)
    {
        _startTime = CFAbsoluteTimeGetCurrent();
    }
    
    _bitCount += 8 * size;
}

- (void) reset
{
    _startTime = -1;
    _lastResolution = CGSizeMake(0, 0);
    _frameCount = 0;
    _bitCount = 0;
}

@end
