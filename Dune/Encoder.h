//
//  Encoder.h
//  Dune
//
//  Created by Andrew Young on 7/16/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>

/**
 * Responsible for compressing raw video frames into H.264 slices.
 */
@interface Encoder : NSObject

/** Specifies average bitrate */
@property NSUInteger bitrate;

/** Hands encoded slices down the pipeline. */
@property (copy) void (^onSlices)(CMSampleBufferRef frame);

/** Encodes a raw video frame */
- (void) encodeFrame:(CMSampleBufferRef)frame;

/** Flushes slices still in buffer */
- (void) flush;

@end
