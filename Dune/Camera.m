//
//  Camera.m
//  Dune
//
//  Created by Andrew Young on 7/15/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import "Camera.h"

@interface Camera()
{
    AVCaptureSession* _session;
    AVCaptureDevice* _device;
    dispatch_queue_t _recordingQueue;
}
@end

@implementation Camera

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        // Default to 720p
        _resolution = VIDEO_RESOLUTION_1280x720;
     
        // Default to 30 fps
        _fps = 30;
        
        // Initialize capture session
        _session = [[AVCaptureSession alloc] init];
        
        // Inputs
        _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        if (_device)
        {
            AVCaptureDeviceInput* input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:NULL];
            [_session addInput:input];
        }
        else
        {
            NSLog(@"Camera - could not access default video capture device");
        }
        
        // Outputs
        AVCaptureVideoDataOutput* output = [[AVCaptureVideoDataOutput alloc] init];
        _recordingQueue = dispatch_queue_create("Recording Q", DISPATCH_QUEUE_SERIAL);
        [output setSampleBufferDelegate:self queue:_recordingQueue];
        
        [_session addOutput:output];
        
        // Rotate frames in hardware
        AVCaptureConnection* connection = [output connectionWithMediaType:AVMediaTypeVideo];
        connection.videoOrientation = AVCaptureVideoOrientationPortrait;
        
        // Register for errors
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(sessionFailed:)
                                                     name:AVCaptureSessionRuntimeErrorNotification
                                                   object:_session];
    }
    return self;
}

- (void) start
{
    // Specify resolution
    switch (self.resolution)
    {
        case VIDEO_RESOLUTION_352x288:
            _session.sessionPreset = AVCaptureSessionPreset352x288;
            break;
        case VIDEO_RESOLUTION_640x480:
            _session.sessionPreset = AVCaptureSessionPreset640x480;
            break;
        case VIDEO_RESOLUTION_1280x720:
            _session.sessionPreset = AVCaptureSessionPreset1280x720;
            break;
        case VIDEO_RESOLUTION_1920x1080:
            _session.sessionPreset = AVCaptureSessionPreset1920x1080;
            break;
        default:
            [NSException raise:@"Invalid resolution"
                        format:@"Resolution %ld is invalid. Please choose from CameraResolution enum", (long)self.resolution];
            break;
    }
    
    // Specify fps
    NSError* error = nil;
    if ([_device lockForConfiguration:&error])
    {
        _device.activeVideoMinFrameDuration = CMTimeMake(1, (int32_t)self.fps);
        _device.activeVideoMaxFrameDuration = CMTimeMake(1, (int32_t)self.fps);
        [_device unlockForConfiguration];
    }
    else
    {
        NSLog(@"Camera - Could not lock device for configuration: %@", error.localizedDescription);
    }
    
    [_session startRunning];
    
    _recording = YES;
}

- (void) stop
{
    [_session stopRunning];

    _recording = NO;
}

- (void) sessionFailed:(NSNotification*)notification
{
    NSError* error = notification.userInfo[AVCaptureSessionErrorKey];
    NSLog(@"Camera - Session failed with error %@", error.localizedDescription);
}

#pragma mark AVCaptureVideoDataOutputSampleBufferDelegate

- (void) captureOutput:(AVCaptureOutput*)captureOutput
 didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
        fromConnection:(AVCaptureConnection*)connection
{
    if (self.onFrame)
    {
        self.onFrame(sampleBuffer);
    }
}

@end
