//
//  PlayerView.m
//  Dune
//
//  Created by Andrew Young on 7/14/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

@import AVFoundation;

#import "PlayerView.h"

@implementation PlayerView

+ (Class) layerClass
{
    return [AVSampleBufferDisplayLayer class];
}

- (void) renderFrame:(CMSampleBufferRef)frame
{
    [((AVSampleBufferDisplayLayer*)self.layer) enqueueSampleBuffer:frame];
}

@end
