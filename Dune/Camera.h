//
//  Camera.h
//  Dune
//
//  Created by Andrew Young on 7/15/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, CameraResolution)
{
    VIDEO_RESOLUTION_352x288,
    VIDEO_RESOLUTION_640x480,
    VIDEO_RESOLUTION_1280x720,
    VIDEO_RESOLUTION_1920x1080
};

/**
 * Responsible for capturing video and sending video frames down the pipeline.
 */
@interface Camera : NSObject<AVCaptureVideoDataOutputSampleBufferDelegate>

/** Specifies video resolution */
@property CameraResolution resolution;

/** Specifies frames per second */
@property NSUInteger fps;

/** Hands raw video frames down the pipeline. */
@property (copy) void (^onFrame)(CMSampleBufferRef frame);

/** Reports recording state */
@property (readonly) BOOL recording;

/**
 * Starts capture.
 */
- (void) start;

/**
 * Stops capture.
 */
- (void) stop;

@end
