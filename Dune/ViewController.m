//
//  ViewController.m
//  Dune
//
//  Created by Andrew Young on 7/14/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import "Camera.h"
#import "Encoder.h"
#import "Statistics.h"
#import "StatisticsViewModel.h"
#import "ViewController.h"

@interface ViewController ()

@property Camera* camera;
@property Encoder* encoder;
@property Statistics* statistics;
@property StatisticsViewModel* statisticsViewModel;

@end

@implementation ViewController

- (void) viewDidLoad
{
    [super viewDidLoad];

    __weak ViewController* weakSelf = self;
    
    // Initialize processing pipeline
    self.camera = [[Camera alloc] init];
    self.encoder = [[Encoder alloc] init];
    
    // Pass raw frames from Camera into Encoder
    self.camera.onFrame = ^(CMSampleBufferRef frame) {
        [weakSelf.encoder encodeFrame:frame];

        // Record stats
        CGSize resolution = CVImageBufferGetDisplaySize(CMSampleBufferGetImageBuffer(frame));
        [weakSelf.statistics capturedFrameWithResolution:resolution];
    };
    
    // Render encoded slices on screen with PlayerView. Later, we'll worry about streaming.
    self.encoder.onSlices = ^(CMSampleBufferRef slices) {
        [weakSelf.playerView renderFrame:slices];
        
        // Record stats
        CMBlockBufferRef blockBuffer = CMSampleBufferGetDataBuffer(slices);
        [weakSelf.statistics encodedSlicesWithSize:CMBlockBufferGetDataLength(blockBuffer)];
    };
    
    // Initialize Statistics and the View Model
    self.statistics = [[Statistics alloc] init];
    self.statisticsViewModel = [[StatisticsViewModel alloc] initWithStatistics:_statistics];
    
    // Clear labels
    self.resolutionLabel.text = @"";
    self.fpsLabel.text = @"";
    self.fpkfLabel.text = @"";
    self.bitrateLabel.text = @"";
    self.latencyLabel.text = @"";
    self.queueLengthLabel.text = @"";
    
    // Schedule label updates
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(updateLabels:) userInfo:nil repeats:YES];
}

- (void) updateLabels:(NSTimer*)timer
{
    self.resolutionLabel.attributedText = self.statisticsViewModel.resolution;
    self.fpsLabel.attributedText = self.statisticsViewModel.fps;
    self.bitrateLabel.attributedText = self.statisticsViewModel.bitrate;
}

- (IBAction) toggleCapture:(id)sender
{
    if (self.camera.recording)
    {
        [self.camera stop];
        [self.statistics reset];
        [self.toggleButton setImage:[UIImage imageNamed:@"record"] forState:UIControlStateNormal];
    }
    else
    {
        [self.camera start];
        [self.toggleButton setImage:[UIImage imageNamed:@"stop"] forState:UIControlStateNormal];
    }
}

@end
