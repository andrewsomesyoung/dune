//
//  StatisticsViewModel.m
//  Dune
//
//  Created by Andrew Young on 7/15/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import "StatisticsViewModel.h"

@interface StatisticsViewModel()
{
    Statistics* _statistics;
}
@end

@implementation StatisticsViewModel

- (instancetype) initWithStatistics:(Statistics*)statistics
{
    self = [super init];
    if (self)
    {
        _statistics = statistics;
    }
    return self;
}

- (NSAttributedString*) resolution
{
    NSString* s = [NSString stringWithFormat:@"%.f x %.f", _statistics.resolution.height, _statistics.resolution.width];
    return [[NSAttributedString alloc] initWithString:s];
}

- (NSAttributedString*) fps
{
    NSString* s = [NSString stringWithFormat:@"%.f fps", _statistics.fps];
    return [[NSAttributedString alloc] initWithString:s];
}

- (NSAttributedString*) bitrate
{
    NSString* s = nil;
    
    if (_statistics.bitrate > 1000000L)
    {
        s = [NSString stringWithFormat:@"%ld Mbps", _statistics.bitrate / 1000000L];
    }
    else if (_statistics.bitrate > 1000)
    {
        s = [NSString stringWithFormat:@"%ld kbps", _statistics.bitrate / 1000];
    }
    else
    {
        s = [NSString stringWithFormat:@"%ld bps", _statistics.bitrate];
    }

    return [[NSAttributedString alloc] initWithString:s];
}

@end
