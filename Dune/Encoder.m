//
//  Encoder.m
//  Dune
//
//  Created by Andrew Young on 7/16/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <VideoToolbox/VideoToolbox.h>
#import "Encoder.h"

// Forwards
static void compressed_slices_cb(void *outputCallbackRefCon,
                                 void *sourceFrameRefCon,
                                 OSStatus status,
                                 VTEncodeInfoFlags infoFlags,
                                 CMSampleBufferRef slices);
@interface Encoder()
{
    VTCompressionSessionRef _session;
}
@end

@implementation Encoder

- (instancetype) init
{
    self = [super init];
    if (self)
    {
        _session = NULL;
        
        // Default to 500 kbps
        self.bitrate = 500000L;
    }
    return self;
}

- (void) encodeFrame:(CMSampleBufferRef)frame
{
    OSStatus status;
    
    if (_session == NULL)
    {
        CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(frame);
        
        status = VTCompressionSessionCreate(NULL,
                                            (int32_t)CVPixelBufferGetWidth(pixelBuffer),
                                            (int32_t)CVPixelBufferGetHeight(pixelBuffer),
                                            kCMVideoCodecType_H264,
                                            NULL,
                                            NULL,
                                            NULL,
                                            compressed_slices_cb,
                                            (__bridge void *)(self),
                                            &_session);
        if (status != noErr)
        {
            NSLog(@"Encoder - VTCompressionSessionCreate error: %d", (int)status);
        }
        
        // Configure session
        VTSessionSetProperty(_session, kVTCompressionPropertyKey_RealTime, kCFBooleanTrue);
        VTSessionSetProperty(_session, kVTCompressionPropertyKey_ProfileLevel, kVTProfileLevel_H264_Baseline_3_1);
        VTSessionSetProperty(_session, kVTCompressionPropertyKey_AverageBitRate, (__bridge CFTypeRef)(@(self.bitrate)));
    }
    
    status = VTCompressionSessionEncodeFrame(_session,
                                             CMSampleBufferGetImageBuffer(frame),
                                             CMSampleBufferGetPresentationTimeStamp(frame),
                                             CMSampleBufferGetDuration(frame),
                                             NULL,
                                             NULL,
                                             NULL);
    if (status != noErr)
    {
        NSLog(@"Encoder - VTCompressionSessionEncodeFrame error: %d", (int)status);
    }
}

- (void) flush
{
    OSStatus status = VTCompressionSessionCompleteFrames(_session, kCMTimeInvalid);
    if (status != noErr)
    {
        NSLog(@"Encoder - VTCompressionSessionCompleteFrames error: %d", (int)status);
    }
    
    VTCompressionSessionInvalidate(_session);
    _session = NULL;
}

- (void) processSlices:(CMSampleBufferRef)slices
{
    if (self.onSlices)
    {
        self.onSlices(slices);
    }
}

@end

void compressed_slices_cb(void *outputCallbackRefCon,
                          void *sourceFrameRefCon,
                          OSStatus status,
                          VTEncodeInfoFlags infoFlags,
                          CMSampleBufferRef slices)
{
    if (status == noErr)
    {
        Encoder* encoder = (__bridge Encoder *)(outputCallbackRefCon);
        [encoder processSlices:slices];
    }
    else
    {
        NSLog(@"Encoder - compressed_slices_cb %d", (int)status);
    }
}

