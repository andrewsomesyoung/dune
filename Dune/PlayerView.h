//
//  PlayerView.h
//  Dune
//
//  Created by Andrew Young on 7/14/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * A custom view that renders raw and/or h.264 encoded frames.
 */
@interface PlayerView : UIView

/**
 * Renders frame on the screen
 */
- (void) renderFrame:(CMSampleBufferRef)frame;

@end
