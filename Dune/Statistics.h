//
//  Statistics.h
//  Dune
//
//  Created by Andrew Young on 7/15/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * A utility that tracks statistics about the video pipeline.
 */
@interface Statistics : NSObject

@property (readonly) CGSize resolution;

@property (readonly) float fps;

@property (readonly) NSUInteger bitrate;

- (void) capturedFrameWithResolution:(CGSize)resolution;

- (void) encodedSlicesWithSize:(NSUInteger)size;

- (void) reset;

@end
