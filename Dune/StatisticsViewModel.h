//
//  StatisticsViewModel.h
//  Dune
//
//  Created by Andrew Young on 7/15/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Statistics.h"

/**
 * A "view model" responsible for formatting the data in a Statistics object.
 */
@interface StatisticsViewModel : NSObject

@property (readonly) NSAttributedString* resolution;

@property (readonly) NSAttributedString* fps;

@property (readonly) NSAttributedString* bitrate;

- (instancetype) initWithStatistics:(Statistics*)statistics;

@end
