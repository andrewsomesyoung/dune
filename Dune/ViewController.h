//
//  ViewController.h
//  Dune
//
//  Created by Andrew Young on 7/14/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerView.h"

@interface ViewController : UIViewController

@property (nonatomic) IBOutlet PlayerView* playerView;

@property (nonatomic) IBOutlet UIButton* toggleButton;

@property (nonatomic) IBOutlet UILabel* resolutionLabel;

@property (nonatomic) IBOutlet UILabel* fpsLabel;

@property (nonatomic) IBOutlet UILabel* fpkfLabel;

@property (nonatomic) IBOutlet UILabel* bitrateLabel;

@property (nonatomic) IBOutlet UILabel* latencyLabel;

@property (nonatomic) IBOutlet UILabel* queueLengthLabel;

/**
 * Toggles video capture.
 */
- (IBAction) toggleCapture:(id)sender;

@end

