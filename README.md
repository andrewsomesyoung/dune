# README

## What is this repository for?

Dune is a tool used to experiment with video compression and live streaming
technology. Using Dune you can experiment with video capture settings,
compression settings, and streaming settings to find an optimal mix that
captures good enough quality video and can be streamed live within your
bandwidth limitations.

The goal is to create a live streaming pipeline that includes:

`Camera -> Encoder -> Publisher -> Broker -> Subscriber -> Decoder -> Player`

The Camera captures raw video frames. The Encoder compresses the raw frames using hardware acceleration into H.264. The Publisher packs the H.264 slices into a bitstream and streams it over the network to a Broker. The Broker is in charge of routing messages from Publishers to Subscribers. The Subscriber downloads the bitstream from the Broker and unpacks it into H.264 slices. The Decoder decompresses the H.264 slices back into raw video frames. Finally, the Player renders the raw video frames on the screen.

## How do I get set up?

Dune uses CocoaPods to manage dependencies. To get started, install CocoaPods from the command line:

`$ sudo gem install cocoapods`

Next, download the project depedencies by running the following from the project directory:

`$ pod install`

CocoaPods will create an Xcode workspace called Dune.xcworkspace. Open this in Xcode and you're good to go!

## Testing

Dune is developed using Test-Driven development practices and includes a suite of unit and integration tests. These can be run from Xcode by choosing Product->Test from the menu.

NOTE: Most of the tests require access to the camera and hardware codecs. While some tests run on the simulator, most require an actual iOS device to run.

## Running

Running Dune gives you a typical camera visual. Once you press record, it starts the pipeline and displays statistics averaged over time.

## Where to next?

The Issues list lays out the planned work as a series of user stories.