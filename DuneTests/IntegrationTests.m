//
//  IntegrationTests.m
//  Dune
//
//  Created by Andrew Young on 7/16/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import "Camera.h"
#import "Statistics.h"

SpecBegin(Integrations)

describe(@"Camera and Statistics", ^{
    
    __block Camera* camera = nil;

#if !TARGET_IPHONE_SIMULATOR // Video capture tests don't work on simulator

    it(@"generate expected fps", ^{

        Statistics* statistics = [[Statistics alloc] init];
        
        // Specify fps
        camera.fps = 30;

        waitUntil(^(DoneCallback done) {

            __block int frameCount = 0;
            
            camera.onFrame = ^(CMSampleBufferRef frame) {
                
                // Record stats
                [statistics capturedFrameWithResolution:CVImageBufferGetDisplaySize(CMSampleBufferGetImageBuffer(frame))];
                
                // Finish test after first 100 frames
                if (++frameCount > 100)
                {
                    done();
                }
            };
            
            [camera start];
        });
        
        // Verify fps
        expect(statistics.fps).to.beCloseToWithin(30, 0.5);
    });
    
#endif
    
    beforeEach(^{
        camera = [[Camera alloc] init];
    });
    
    afterEach(^{
        [camera stop];
    });
});

SpecEnd