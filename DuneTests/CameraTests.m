//
//  CameraTests.m
//  Dune
//
//  Created by Andrew Young on 7/15/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import "Camera.h"

SpecBegin(Camera)

describe(@"Camera", ^{
    
    __block Camera* camera = nil;

    it(@"reports recording state", ^{
        
        expect(camera.recording).to.equal(NO);
        
        [camera start];

        expect(camera.recording).to.equal(YES);
        
        [camera stop];
        
        expect(camera.recording).to.equal(NO);

    });
    
#if !TARGET_IPHONE_SIMULATOR // Video capture tests don't work on simulator
    
    it(@"generates at least one frame", ^{
        waitUntil(^(DoneCallback done) {
            camera.onFrame = ^(CMSampleBufferRef frame) {
                done();
            };
            
            [camera start];
        });
    });
    
    it(@"honors 352x288 resolution", ^{
        waitUntil(^(DoneCallback done) {
            
            // Specify resolution
            camera.resolution = VIDEO_RESOLUTION_352x288;
            
            camera.onFrame = ^(CMSampleBufferRef frame) {
                
                CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(frame);
                CGSize size = CVImageBufferGetDisplaySize(imageBuffer);
                
                // Check that frame is 288 x 352 - width and height are flipped because it's
                // always in portrait mode
                expect(size.width).to.equal(288);
                expect(size.height).to.equal(352);
                
                done();
            };
            
            [camera start];
        });
    });
    
    it(@"honors 1920x1080 resolution", ^{
        waitUntil(^(DoneCallback done) {
            
            // Specify resolution
            camera.resolution = VIDEO_RESOLUTION_1920x1080;
            
            camera.onFrame = ^(CMSampleBufferRef frame) {
                
                CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(frame);
                CGSize size = CVImageBufferGetDisplaySize(imageBuffer);
                
                // Check that frame is 1080 x 1920 - width and height are flipped because it's
                // always in portrait mode
                expect(size.width).to.equal(1080);
                expect(size.height).to.equal(1920);
                
                done();
            };
            
            [camera start];
        });
    });
    
    it(@"honors fps", ^{
        
        CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
        __block int frameCount = 0;

        // Specify fps
        camera.fps = 5;
        
        camera.onFrame = ^(CMSampleBufferRef frame) {
            frameCount++;
        };
        
        [camera start];
        
        // Wait until we've captured 10 frames
        while (frameCount < 10)
        {
            usleep(100000L); // 100ms
        }
        
        // Verify fps
        CFAbsoluteTime delta = CFAbsoluteTimeGetCurrent() - startTime;
        expect(delta).to.beCloseToWithin(2.0, 0.3);
    });    
    
    it(@"raises exception on invalid resolution", ^{
        
        // Specify invalid camera resolution
        camera.resolution = 1000;
        
        expect(^{ [camera start]; }).to.raiseAny();
    });
    
#endif
    
    beforeEach(^{
        camera = [[Camera alloc] init];
    });
    
    afterEach(^{
        [camera stop];
    });
});

SpecEnd



