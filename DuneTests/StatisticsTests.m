//
//  StatisticsTests.m
//  Dune
//
//  Created by Andrew Young on 7/15/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import "Statistics.h"

SpecBegin(Statistics)

describe(@"Statistics", ^{
    
    it(@"tracks resolution", ^{
        
        Statistics* statistics = [[Statistics alloc] init];
        
        // Capture frames with one resolution
        CGSize resolution = CGSizeMake(1080, 1920);
        [statistics capturedFrameWithResolution:resolution];
        expect(statistics.resolution.width).to.equal(1080);
        expect(statistics.resolution.height).to.equal(1920);
        
        // Capture frames with another resolution
        resolution = CGSizeMake(10, 20);
        [statistics capturedFrameWithResolution:resolution];
        expect(statistics.resolution.width).to.equal(10);
        expect(statistics.resolution.height).to.equal(20);

    });
    
    it(@"counts frames", ^{

        CGSize resolution = CGSizeMake(1080, 1920);
        
        Statistics* statistics = nil;
        
        // Emulate 5 fps
        statistics = [[Statistics alloc] init];
        for (int i = 0;i < 5;i++)
        {
            [statistics capturedFrameWithResolution:resolution];
            usleep(1000000L / 5);
        }
        expect(statistics.fps).to.beCloseToWithin(5, 0.5);
        
        // Emulate 10 fps
        statistics = [[Statistics alloc] init];
        for (int i = 0;i < 10;i++)
        {
            [statistics capturedFrameWithResolution:resolution];
            usleep(1000000L / 10);
        }
        expect(statistics.fps).to.beCloseToWithin(10, 0.5);
        
    });
    
    it(@"counts bitrate", ^{

        Statistics* statistics = nil;

        // Emulate 40 kbps
        statistics = [[Statistics alloc] init];
        for (int i = 0;i < 5;i++)
        {
            [statistics encodedSlicesWithSize:1000];
            usleep(1000000L / 5);
        }
        expect(statistics.bitrate).to.beCloseToWithin(40000, 300);
        
    });
    
});

SpecEnd
