//
//  StatisticsViewModelTests.m
//  Dune
//
//  Created by Andrew Young on 7/15/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <OCMock/OCMock.h>
#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import "StatisticsViewModel.h"

SpecBegin(StatisticsViewModel)

describe(@"StatisticsViewModel", ^{
    
    it(@"formats resolution", ^{
        
        // Create a Statistics object with known resolution
        Statistics* statistics = OCMClassMock([Statistics class]);
        OCMStub([statistics resolution]).andReturn(CGSizeMake(1080, 1920));
        
        StatisticsViewModel* viewModel = [[StatisticsViewModel alloc] initWithStatistics:statistics];
        
        // Verify width and height are swapped and formatted correctly
        expect(viewModel.resolution.string).to.equal(@"1920 x 1080");
        
    });
    
    it(@"formats fps", ^{
        
        // Create a Statistics object with known fps
        Statistics* statistics = OCMClassMock([Statistics class]);
        OCMStub([statistics fps]).andReturn(30);
        
        StatisticsViewModel* viewModel = [[StatisticsViewModel alloc] initWithStatistics:statistics];
        
        // Verify formatting
        expect(viewModel.fps.string).to.equal(@"30 fps");
        
    });
    
    it(@"formats bitrate", ^{

        // Create a Statistics object with known bitrate in bps
        Statistics* statistics = OCMClassMock([Statistics class]);
        OCMStub([statistics bitrate]).andReturn(800);
        
        StatisticsViewModel* viewModel = [[StatisticsViewModel alloc] initWithStatistics:statistics];
        
        // Verify formatting
        expect(viewModel.bitrate.string).to.equal(@"800 bps");

        // Create a Statistics object with known bitrate in kbps
        statistics = OCMClassMock([Statistics class]);
        OCMStub([statistics bitrate]).andReturn(800000);
        
        viewModel = [[StatisticsViewModel alloc] initWithStatistics:statistics];
        
        // Verify formatting
        expect(viewModel.bitrate.string).to.equal(@"800 kbps");
        
        // Create a Statistics object with known bitrate in Mbps
        statistics = OCMClassMock([Statistics class]);
        OCMStub([statistics bitrate]).andReturn(4000000);
        
        viewModel = [[StatisticsViewModel alloc] initWithStatistics:statistics];
        
        // Verify formatting
        expect(viewModel.bitrate.string).to.equal(@"4 Mbps");
        
    });
    
});

SpecEnd