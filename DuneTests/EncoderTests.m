//
//  EncoderTests.m
//  Dune
//
//  Created by Andrew Young on 7/16/15.
//  Copyright (c) 2015 Andrew Young. All rights reserved.
//

#import <Specta/Specta.h>
#import <Expecta/Expecta.h>
#import "Camera.h"
#import "Encoder.h"

SpecBegin(Encoder)

// TODO: (ASY 07/16/15) It would be great to find a way to generate test frames without using the Camera

describe(@"Encoder", ^{
    
    __block Camera* camera = nil;
    __block Encoder* encoder = nil;
    
#if !TARGET_IPHONE_SIMULATOR // Video capture tests don't work on simulator

    it(@"generates at least one slice", ^{
        
        // Pass frames from Camera into Encoder
        camera.onFrame = ^(CMSampleBufferRef frame) {
            [encoder encodeFrame:frame];
        };
        
        waitUntil(^(DoneCallback done) {
            encoder.onSlices = ^(CMSampleBufferRef slices) {
                done();
            };

            [camera start];
        });
        
        [camera stop];
    });
    
    it(@"honors bitrate", ^{
        
        CFAbsoluteTime startTime = CFAbsoluteTimeGetCurrent();
        __block int sliceCount = 0;
        __block int bitCount = 0;

        // Pass frames from Camera into Encoder
        camera.onFrame = ^(CMSampleBufferRef frame) {
            [encoder encodeFrame:frame];
        };

        // Specify bitrate
        encoder.bitrate = 100000L;
        
        encoder.onSlices = ^(CMSampleBufferRef slices) {
            CMBlockBufferRef blockBuffer = CMSampleBufferGetDataBuffer(slices);
            bitCount += 8 * CMBlockBufferGetDataLength(blockBuffer);
            
            sliceCount++;
        };
        
        [camera start];
        
        // Wait until we've captured 10 slices
        while (sliceCount < 10)
        {
            usleep(100000L); // 100ms
        }
        
        // Verify bitrate
        CFAbsoluteTime delta = CFAbsoluteTimeGetCurrent() - startTime;
        expect(bitCount / delta).to.beCloseToWithin(encoder.bitrate, 10000);
    });
    
#endif
    
    beforeEach(^{
        camera = [[Camera alloc] init];
        encoder = [[Encoder alloc] init];
    });
    
    afterEach(^{
        [camera stop];
        [encoder flush];
    });

});

SpecEnd
